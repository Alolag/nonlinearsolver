CXX = gcc
CXXFLAGS = -std=c++14 -Wno-c99-extensions -Wall -pthread -Wextra -pedantic -Wconversion -g3 -march=native -O2 -isystem /usr/include/eigen3 -lgsl -lm -lstdc++fs -fopenmp -lstdc++ # -fsanitize=memory

SRC = src/
build_dir = build/

vpath %.cc $(SRC)
vpath %.h $(SRC)

all: main
	$(guile (chdir "$(build_dir)"))
	./a help

main: $(addprefix $(build_dir),main.o conv.o)
	$(guile (if (not (file-exists? "$(build_dir)")) (mkdir "$(build_dir)")))
	$(CXX) $^ $(CXXFLAGS) -o $(build_dir)a

$(build_dir)%.o: %.cc %.h
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(build_dir)%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	$(RM) -rf $(build_dir)*.o $(build_dir)a
